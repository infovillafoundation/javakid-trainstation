package org.bitbucket.infovillafoundction.javakid.trainschedule;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGen {
    public static void main(String[] args) {
        Schema schema=new Schema(1, "org.bitbucket.infovillafoundation.javakid.trainschedule.dao");
        Entity trainType;
        trainType = schema.addEntity("TrainType");
        trainType.addIdProperty();
        trainType.addStringProperty("typeName");
        trainType.addStringProperty("typeNameMyanmar");

        Entity train = schema.addEntity("Train");

        train.addIdProperty();
        train.addStringProperty("myanmarName");
        train.addStringProperty("englishName");
        train.addIntProperty("price");
        train.addStringProperty("image");
        Property trainTypeIdProperty = train.addLongProperty("trainTypeId").getProperty();
        train.addToOne(trainType,trainTypeIdProperty);

        try {
            new DaoGenerator().generateAll(schema, "app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
