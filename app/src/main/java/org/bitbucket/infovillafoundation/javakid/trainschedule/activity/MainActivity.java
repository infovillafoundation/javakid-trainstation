package org.bitbucket.infovillafoundation.javakid.trainschedule.activity;

import android.app.Activity;
import android.os.Bundle;

import org.bitbucket.infovillafoundation.javakid.trainschedule.R;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
