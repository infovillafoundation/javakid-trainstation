package org.bitbucket.infovillafoundation.javakid.trainschedule.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import org.bitbucket.infovillafoundation.javakid.trainschedule.dao.TrainType;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table TRAIN_TYPE.
*/
public class TrainTypeDao extends AbstractDao<TrainType, Long> {

    public static final String TABLENAME = "TRAIN_TYPE";

    /**
     * Properties of entity TrainType.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property TypeName = new Property(1, String.class, "typeName", false, "TYPE_NAME");
        public final static Property TypeNameMyanmar = new Property(2, String.class, "typeNameMyanmar", false, "TYPE_NAME_MYANMAR");
    };


    public TrainTypeDao(DaoConfig config) {
        super(config);
    }
    
    public TrainTypeDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'TRAIN_TYPE' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'TYPE_NAME' TEXT," + // 1: typeName
                "'TYPE_NAME_MYANMAR' TEXT);"); // 2: typeNameMyanmar
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'TRAIN_TYPE'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, TrainType entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String typeName = entity.getTypeName();
        if (typeName != null) {
            stmt.bindString(2, typeName);
        }
 
        String typeNameMyanmar = entity.getTypeNameMyanmar();
        if (typeNameMyanmar != null) {
            stmt.bindString(3, typeNameMyanmar);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public TrainType readEntity(Cursor cursor, int offset) {
        TrainType entity = new TrainType( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // typeName
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2) // typeNameMyanmar
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, TrainType entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setTypeName(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setTypeNameMyanmar(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(TrainType entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(TrainType entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
